<?php
namespace App\Http\Controllers;

// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Credentials: true');
// header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\User;
use App\Link;

class AuthController extends Controller
{
    /**
     * 
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function getLinks(Request $request)
    {

        //"select * from `links` where `url_key` LIKE ? / this fo extensions"

        $links = DB::table('links')->where('url_key', 'like', '%' . $request->search . '%')
            ->get();
        //toSql();//Link::where('url_key', 'LIKE', "%$request->param%")->toSql();

        return response()->json($links);
    }
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'type' => 'required|string|max:255',
        ]);
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'type' => $request->type
        ]);
        $user->save();
        return response()->json([
            'message' => $request->type
        ], 201);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);


        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'user' => $user,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    /** start crud application */
    public function postLinks(Request $request)
    {
        request()->validate([
            'url_key' => 'required',
            'url' => 'required'
        ]);
        Link::create($request->all());
        // $response->headers->set('Content-Type', 'application/json');
        // $response->headers->set('Access-Control-Allow-Origin', '*');
        return response()->json($request);
    }


    public function postUpdate(Request $request, $id)
    {
        request()->validate([
            'url_key' => 'required',
            'url' => 'required'
        ]);
        $links = Link::find(['id' => $id])[0];
        $links->url_key = $request->url_key;
        $links->url = $request->url;
        $links->save();

        return response()->json($request);
    }


    public function getAllLinks()
    {
        $links = Link::latest()->paginate(5);

        return response()->json($links);
    }
    public function delete($id)
    {
        $links = Link::find(['id' => $id])[0];
        $links->delete();
        return response()->json($links);
    }
}
