<?php

namespace App\Http\Controllers;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
use Illuminate\Support\Facades\DB;
use App\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLinks(Request $request)
    {

        //"select * from `links` where `url_key` LIKE ? / this fo extensions"

        $links = DB::table('links')->where('url_key', 'like', '%' . $request->search . '%')
            ->get();
        //toSql();//Link::where('url_key', 'LIKE', "%$request->param%")->toSql();

        return response()->json($links);
    }

    // public function postLinks(Request $request)
    // {
    //     request()->validate([
    //         'url_key' => 'required',
    //         'url' => 'required'
    //     ]);
    //     Link::create($request->all());
    //     // $response->headers->set('Content-Type', 'application/json');
    //     // $response->headers->set('Access-Control-Allow-Origin', '*');
    //     return response()->json($request);
    // }
    public function index()
    {
        //this for web pages 
        $links = Link::latest()->paginate(5);
        return view('links.index', compact('links'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('links.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'url_key' => 'required',
            'url' => 'required'
        ]);
        Link::create($request->all());
        return redirect()->route('links.index')->with('success', 'Link created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function show(Link $link)
    {
        $link = Link::find($link)[0];

        return view('links.show', compact('link'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function edit(Link $link)
    {
        $link = Link::find($link)[0];

        return view('links.edit', compact('link'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Link $link)
    {

        request()->validate([
            'url_key' => 'required',
            'url' => 'required'
        ]);
        Link::find($link)[0]->update($request->all());
        return redirect()->route('links.index')->with('success', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Link  $link
     * @return \Illuminate\Http\Response
     */
    public function destroy(Link $link)
    {
        Link::find($link)[0]->delete();
        return redirect()->route('links.index')->with('success', 'Post deleted successfully');
        //
    }
}
