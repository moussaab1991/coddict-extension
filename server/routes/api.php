<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/links', "LinkController@getLinks");


Route::group([
    'prefix' => 'auth'
], function () {

    Route::post('login', 'AuthController@login');


    Route::group([
        'middleware' => 'auth:api'
    ], function () {

        Route::post('/links', "AuthController@getLinks");
        Route::post('signup', 'AuthController@signup');
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');

        Route::get('url', "AuthController@getAllLinks");
        Route::delete('/delete/{id}', "AuthController@delete");

        Route::post('/update/{id}', "AuthController@postUpdate");
        Route::post('/key', "AuthController@postLinks");
    });
});
