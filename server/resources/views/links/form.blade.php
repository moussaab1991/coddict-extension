<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <strong>Key : </strong>
            {!! Form::text('url_key',null,['placeholder'=>'Key','class'=>'form-control']) !!}
        </div>
    </div>

    <div class="col-xs-12">
            <div class="form-group">
                <strong>URL : </strong>
                {!! Form::text('url',null,['placeholder'=>'https://www.','class'=>'form-control']) !!}
            </div>
    </div>
    <div class="col-xs-12">
    <a class="btn btn-xs btn-success" href="{{ route('links.index')}}">Back</a>
        <button type="submit" class="btn btn-xs btn-primary" name="button">Submit</button>
    </div>
</div>