@extends('links.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
        <h3>Show Link</h3> <a class ="btn btn-xs btn-primary"href="{{ route('links.index')}}">Back</a>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <strong>Key :</strong>
            {{$link->url_key}}
        </div>
    </div>
    <div class="col-xs-12">
            <div class="form-group">
                <strong>URL :</strong>
                {{$link->url}}
            </div>
        </div>

</div>

</div>
    
@endsection