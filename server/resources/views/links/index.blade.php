@extends('links.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3>Links Editing</h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="pull-right">
        <a class="btn btn-xs btn-success" href="{{route('links.create')}}">Create new Links</a></div>
        
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
<p>{{$message}}</p>
</div>
    
@endif
<table class="table table-bordered">
    <tr>
        <th>No.</th>
        <th>Links</th>
        <th>Url</th>
        <th width="300px">Actions</th>
    </tr>

    @foreach ($links as $link)
    <tr>
        <td>{{++$i}}</td>
        <td>{{$link->url_key}}</td>
        <td>{{$link->url}}</td>
        <td>
        <a class="btn btn-x btn-info" href="{{route('links.show',$link->id)}}">Show</a>
        <a class="btn btn-x btn-info" href="{{route('links.edit',$link->id)}}">Edit</a>
        {!! Form::open(['method'=>'DELETE','route'=>['links.destroy',$link->id],'style'=>'display:inline'])!!}
        {!! Form::submit('Delete',['class'=>'btn btn-xs btn-danget']) !!}
        {!! Form::close() !!}
        </td>
    </tr>
        
    @endforeach

</table>
    {!! $links->links() !!}
@endsection