@extends('links.master')
 @section('content')
     <div class="row">
         <div class="col-lg-12">
             <div class="pull-left">
                <h3>Edit New Link</h3>
             </div>
         </div>
     </div>
     @if (count($errors)>0)
     <div class="alert alert-danger">
         <strong>Whoooops!!!</strong> There wewre some problems with your input.<br>
         <ul>
             @foreach ($errors->all() as $error)
         <li>{{$error}}</li>                 
             @endforeach
         </ul>
     </div>
         
     @endif

     {!! Form::model($link,['method'=> 'PATCH','route'=>['links.update',$link]])!!}
     @include('links.form')
     {!! Form::close() !!}

 @endsection